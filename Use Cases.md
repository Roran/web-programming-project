# Список акторов

## User
**Primary Actor.** Пользователь приложения. Ему доступны такие базовые функции как:
- вход в систему;
- выбор уровня физической подготовки. 

Данный актор является родительским и ему доступен минимальный набор функционала, не зависящий от уровня физической подготовки.

## Low Level User
**Primary Actor.** Пользователь низкого уровня. Наследует поведение актора [User](https://gitlab.com/Roran/web-programming-project/-/blob/main/Use%20Cases.md#user). Так как пользователь с низким уровнем скорее всего не знаком со спортивной теорией и может не знать всех тонкостей тренировок, их особенности выбираются за него. Пользователю низкого уровня выдаются ежедневные "квесты" (набор упражнений), которые он может отметить как выполненные и получить за это опыт. При достижении определенного численного уровня переходит в категорию среднего уровня.

## Middle Level User
**Primary Actor.** Пользователь среднего уровня. Наследует поведелние актора [Low Level User](https://gitlab.com/Roran/web-programming-project/-/blob/main/Use%20Cases.md#low-level-user). Подразумевается что данный пользователь понимает свои спортивные цели может выбирать направление в котором двигаться. Направлением может быть:
- Гибкость;
- Выносливость;
- Физическая сила.

Дневные квесты определяются программой на основе ежеденвного выбора направления подготовки. При достижении определенного числового уровня переходит в категорию высокого уровня.

## High Level User
**Primary Actor.** Пользователь высокого уровня. Наследует поведелние актора [Middle Level User](https://gitlab.com/Roran/web-programming-project/-/blob/main/Use%20Cases.md#middle-level-user). Данный пользователь рассматривается как опытный в сфере спорта, так что он может определить не только направление, в котором хочет двигаться, но и какие группы мышц развивать. Содержимое дневных квестов также определяются программой на основе этих выборов.

## Data Base
**Secondary Actor.** Актор отображающий базу данных, выполняет действия на основе запросов пользователя. Доступный функционал - проверка пароля и работа с достижениями. 

# Диаграмма прецедентов использования
<p align="center">
  <img src="../Document Images/UML_UseCaseDiagram.png" width="1000" title="hover text">
</p>

# Спецификации прецедентов использования

## Log in

<p align="center">
  <img src="../Document Images/LogIn.png" width="1000" title="hover text">
</p>

## Choose Fitness Level

<p align="center">
  <img src="../Document Images/Fitness_level.png" width="1000" title="hover text">
</p>

## Check Done Quest Activity 

<p align="center">
  <img src="../Document Images/Done_Quest.png" width="1000" title="hover text">
</p>

## Choose Daily Type of WorkOut

<p align="center">
  <img src="../Document Images/Type_Of_Workout.png" width="1000" title="hover text">
</p>

## Choose Muscle Group of WorkOut

<p align="center">
  <img src="../Document Images/Muscle_Group.png" width="1000" title="hover text">
</p>
