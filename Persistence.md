# Список сущностей, свойств и связей

<p align="center">
  
  <img src="../Document Images/TableOfEntities.png" width="900" title="hover text">
  
  ## Связи между сущностями

  - Связь один к одному между User и Statistics - каждому пользователю соответствует единый набор статистических данных, и каждый набор статистики уникален для пользователя;
  - Связь один ко многим между User и Achievments - каждому пользователю может выполнить несколько достижений (или ни выполнить ни одного);
  - Связь один ко многим между User и Quest - каждый пользователь может выполнить несколько квестов (или ни выполнить ни одного);
  - Связь один ко многим между Quest и Exercise - в одном квесте несколько упражнений (как минимум одно);
</p>

# ER - диаграмма

<p align="center">
  <img src="../Document Images/ER_Diagram.png" width="1000" title="hover text">
  
</p>

# Даталогическая модель

<p align="center">
  <img src="../Document Images/Datalog_model.png" width="1000" title="hover text">
  
</p>
