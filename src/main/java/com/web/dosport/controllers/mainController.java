package com.web.dosport.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class mainController {

//    private UserRepo userRepo;
//
//    //конструктор принимающий^
//
//
//    public mainController(UserRepo userRepo) {
//        this.userRepo = userRepo;
//    }

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("login", "Вход в систему");
        return "login";
    }
    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("main", "Главная страница");
        return "main";
    }
    @GetMapping("/achievements")
    public String achievements(Model model) {
        model.addAttribute("achieve", "Достижения");
        return "achieve";
    }
    @GetMapping("/rating")
    public String rating(Model model) {
        model.addAttribute("rating", "Достижения");
        return "rating";
    }
    @GetMapping("/profile")
    public String profile(Model model) {
        model.addAttribute("login", "Профиль");
        return "profile";
    }

    @GetMapping("/questtable")
    public String questtable(Model model) {
        model.addAttribute("questtable", "Таблица квестов");
        return "questtable";
    }

    @GetMapping("/authorisation")
    public String authorisation(Model model) {
        model.addAttribute("authorisation", "Регистрация");
        return "authorisation";
    }
}
