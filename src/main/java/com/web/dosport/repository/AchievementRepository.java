package com.web.dosport.repository;

import com.web.dosport.entity.Achievement;
import org.springframework.data.repository.CrudRepository;

public interface AchievementRepository extends CrudRepository<Achievement, Long> {
}
