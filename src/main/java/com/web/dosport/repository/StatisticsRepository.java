package com.web.dosport.repository;

import com.web.dosport.entity.Statistics;
import org.springframework.data.repository.CrudRepository;

public interface StatisticsRepository extends CrudRepository<Statistics, Long> {
}
