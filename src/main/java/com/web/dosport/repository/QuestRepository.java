package com.web.dosport.repository;

import com.web.dosport.entity.Quest;
import org.springframework.data.repository.CrudRepository;

public interface QuestRepository extends CrudRepository<Quest, Long> {
}
