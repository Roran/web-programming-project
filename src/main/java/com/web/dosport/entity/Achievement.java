package com.web.dosport.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Achievement {

    @Id
    private Long id;
    private String name;
    private int reachpoint;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReachpoint() {
        return reachpoint;
    }

    public void setReachpoint(int reachpoint) {
        this.reachpoint = reachpoint;
    }
}
