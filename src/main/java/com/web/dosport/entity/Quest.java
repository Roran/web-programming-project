package com.web.dosport.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Quest {

    @Id
    private Long id;
    private String name;
    private int level;
    private int exp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }
}
