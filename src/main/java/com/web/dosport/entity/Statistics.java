package com.web.dosport.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Statistics {

    @Id
    private Long id;
    private int achievementcount;
    private int questcount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAchievementcount() {
        return achievementcount;
    }

    public void setAchievementcount(int achievementcount) {
        this.achievementcount = achievementcount;
    }

    public int getQuestcount() {
        return questcount;
    }

    public void setQuestcount(int questcount) {
        this.questcount = questcount;
    }
}
